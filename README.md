# Vinyl Crud
Application CRUD pour des disques vinyl


### Pré-requis

Ce qu'il est requis pour commencer avec le projet...

- PHP
- MySQL
- Docker

### Installation

Les étapes pour installer l'application :

* Cloner le projet
* Se placer dans le projet
* Lancer les conteneurs Docker avec `docker-compose up -d`